package com.example.cap_padron15_ver.almacenapps;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cap_padron15_ver on 15/06/2015.
 */
public class pruebaapp extends AppCompatActivity{
    datosestaticos obj= new datosestaticos();
    private long enqueue;
    private long id;
    // private DownloadManager dm;
    ProgressBar pb;
    Dialog dialog;
    int downloadedSize = 0;
    int totalSize = 0;
    TextView cur_val;
    Button descargar;
    int len1;
    ProgressDialog progress;
    private static final String DL_ID = "downloadId";
    private SharedPreferences prefs;
    private DownloadManager dm;
    ProgressBar progressBar;
    int progressStatus = 0;
    Runnable listener;
    TextView prg;
    private Handler handler2 = new Handler();
    Context context;
    private PackageManager packageManager = null;
    float total,currentdown;
    infoappsintall dapp= new infoappsintall();
    //  private VersionChecker mVC = new VersionChecker();
    // Elementos de la interfaz
    private View mActualizada, mActualizacion, mProgressBar;
    private TextView mVersionActual, mVersionNueva;
    private Button mObtenerDatos, mDescargar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appinfo);

        context = this;
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.back);
        actionBar.setDisplayHomeAsUpEnabled(true);

        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        TextView nombre=(TextView)findViewById(R.id.nom);
        TextView version=(TextView)findViewById(R.id.ver);
        TextView fecha=(TextView)findViewById(R.id.act);
        prg=(TextView)findViewById(R.id.progre);
        ImageView pict=(ImageView)findViewById(R.id.icono);
        descargar=(Button)findViewById(R.id.descarga);
        TextView observacion=(TextView)findViewById(R.id.obs);
        TextView and_ver=(TextView)findViewById(R.id.version_android);
        //TextView enlace= (TextView)findViewById(R.id.liga);
        Button abrir=(Button)findViewById(R.id.abrir);
        //  enlace.setText(Html.fromHtml("<a href=http://www.Depstore.gear.host>http://www.Depstore.gear.host</a>"));
        nombre.setText(obj.getNombre());
        version.setText(obj.getVersion());
        fecha.setText(obj.getFecha());
        observacion.setText(obj.getDescrip());
        and_ver.setText(obj.getAndroid_version());
        packageManager = getPackageManager();
        if(getInstalledApps(false,obj.getNombre())){
            descargar.setText("Instalada");
            descargar.setEnabled(false);
            abrir.setVisibility(View.VISIBLE);
            if(dapp.getNombre().equals(obj.getNombre()) && !dapp.getVersionname().equals(obj.getVersion())) {
                descargar.setText("Actualizar");
                descargar.setEnabled(true);
            }
        }else{
            descargar.setEnabled(true);
            descargar.setText("Instalar");
        }

        new DownloadImageTask(pict)
                .execute("http://agendaws.gearhostpreview.com/Depstore/imagen.php?id=" + obj.getIdaplicacion());

        descargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 //descargaarchivos();
              //  descargar.setBackgroundColor(Color.parseColor("#ff3f782c"));
               // startActivity(new Intent("android.intent.action.VIEW", Uri.parse(obj.getUrl())));
                //listener = OnFinishRunnable;
                descargar.setText("Descargando");
                descargar.setEnabled(false);
                prg.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                String params[] = {obj.getUrl()};
                downloadInstaller.execute(params);
            }
        });
        abrir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = packageManager
                            .getLaunchIntentForPackage(dapp.getPaquete());

                    if (null != intent) {
                        startActivity(intent);
                    }
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(pruebaapp.this, e.getMessage(),
                            Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Toast.makeText(pruebaapp.this, e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage){
            this.bmImage = bmImage;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;

            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_install, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if ((id==android.R.id.home)) {

            Intent intent = new Intent(getApplication(), MainActivity.class);
            intent.putExtra("finish", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities*/
            startActivity(intent);
            pruebaapp.this.finish();
            // app icon in action bar clicked; goto parent activity.
            return true;
        }
        if ((id==R.id.instalar)) {

            Intent intent = new Intent();
            intent.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
            startActivity(intent);
            // app icon in action bar clicked; goto parent activity.
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void descargaarchivos() {
        String dir = Environment.DIRECTORY_DOWNLOADS;

        File fileDir = new File(dir);
        if (!fileDir.isDirectory()) {
            fileDir.mkdir();
        }

       /* File direct = new File(Environment.getExternalStorageDirectory()
                + "/DepStore");

        if (!direct.exists()) {
            direct.mkdirs();
        }*/
        // Toast.makeText(Detail.this, "Download song " + name, Toast.LENGTH_SHORT).show();
        // Download File
        DownloadManager.Request request =
                new DownloadManager.Request(Uri.parse(obj.getUrl()));
        request.setDescription(obj.getApk());
        request.setTitle(obj.getNombre());
        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalPublicDir(dir, obj.getApk());
        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);

    }
    // Handler para la actualización de la interfaz desde el hilo en segundo
    // plano
    private Handler handler = new Handler();

    // Runnable que se ejecutará desde el hilo principal cuando acabe la
    // obtención de datos en segundo plano. Equivalente al onPostExecute() de AsyncTask
    private Runnable finishBackgroundDownload = new Runnable() {
        @Override
        public void run() {
            mProgressBar.setVisibility(View.GONE);
          /*  if(mVC.isNewVersionAvailable()){
                // Hay una nueva versión disponible
                mActualizacion.setVisibility(View.VISIBLE);
                mActualizada.setVisibility(View.GONE);
                // Mostramos los datos
                mVersionActual.setText("Versión actual: " + mVC.getCurrentVersionName());
                mVersionNueva.setText("Versión disponible: " + mVC.getLatestVersionName());
            }else{
                // La aplicación está actualizada
                mActualizacion.setVisibility(View.GONE);
                mActualizada.setVisibility(View.VISIBLE);
            }*/
        }
    };

    // Runnable encargado de descargar los datos en un hilo en segundo plano.
    // Equivalente al doInBackground() de AsyncTask
    private Runnable backgroundDownload = new Runnable() {
        @Override
        public void run() {
          //  mVC.getData(pruebaapp.this);
            // Cuando acabe la descarga actualiza la interfaz
            handler.post(finishBackgroundDownload);
        }
    };


    class  detalleapp  {
        private  String appname =  "" ;
        private  String pname =  "" ;
        private  String versionName =  "" ;
        private  int versionCode =  0 ;
        private Drawable icon ;
        private  void prettyPrint ()  {
            //Log.v(appname + "t" + pname + "t" + versionName + "t" + versionCode);
        }
    }



    private  boolean getInstalledApps ( boolean getSysPackages, String nom )  {
        ArrayList< detalleapp > res =  new  ArrayList < detalleapp >();
        List<PackageInfo> packs = getPackageManager (). getInstalledPackages(0);
        String m="";
        for ( int i = 0 ; i < packs . size (); i ++)  {
            PackageInfo p = packs . get ( i );
            if  ((! getSysPackages )  &&  ( p . versionName ==  null ))  {
                continue  ;
            }
            detalleapp newInfo =  new  detalleapp ();
            newInfo.appname = p . applicationInfo . loadLabel ( getPackageManager ()). toString ();
            dapp.setNombre(newInfo.appname);
            newInfo.pname = p . packageName ;
            dapp.setPaquete(newInfo.pname);
            newInfo.versionName = p . versionName ;
            dapp.setVersionname(newInfo.versionName);
            newInfo.versionCode = p . versionCode ;
            newInfo.icon = p . applicationInfo . loadIcon ( getPackageManager ());
            if(newInfo.appname.equals(nom)) {
                m="0";
                i=packs.size();
            }
            //res . add ( newInfo );
        }
        if(m=="0") {
            return true;
        }else
            return false;
        //return res ;
    }


    @SuppressLint("StaticFieldLeak")
    private AsyncTask<String, Integer, Intent> downloadInstaller = new AsyncTask<String, Integer, Intent>() {
        @Override
        protected Intent doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                String PATH = Environment.getExternalStorageDirectory()
                        + "/DepStore";
                Log.e("Path", PATH);
                File file = new File(PATH);
                file.mkdirs();
                final File outputFile = new File(file,obj.getApk());
                final FileOutputStream fos = new FileOutputStream(outputFile);

                final InputStream is = c.getInputStream();
                totalSize = c.getContentLength();
                final byte[] buffer = new byte[1024];
                len1 = 0;
                currentdown=0;
                total=0;

                runOnUiThread(new Runnable() {
                    public void run() {
                        progressBar.setMax(totalSize);
                    }
                });

                while ((len1 = is.read(buffer)) != -1) {
                                fos.write(buffer, 0, len1);
                                downloadedSize += len1;
                                // Update the progress bar and display the
                                //current value in the text view
                                handler2.post(new Runnable() {
                                    public void run() {
                                        progressBar.setProgress(downloadedSize);
                                        float per = ((float)downloadedSize/totalSize) * 100;
                                        total=totalSize/100000;
                                        currentdown=downloadedSize/100000;
                                        currentdown=currentdown/10;
                                        total=total/10;
                                        prg.setText("Descargando " + currentdown + "Mb / " + total + "Mb  (" + (int)per + "%)" );
                                    }
                                });
                                try {
                                    // Sleep for 200 milliseconds.
                                    //Just to display the progress slowly
                                    Thread.sleep(0);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            fos.close();
                            is.close();
                /*while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);

                }*/
              //  fos.close();
              //  is.close();//till here, it works fine - .apk is download to my sdcard in download file
                //Toast.makeText(getApplication(),"Instalando",Toast.LENGTH_LONG).show();
                runOnUiThread(new Runnable() {
                    public void run() {
                        prg.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    }
                });

                /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(), "application/vnd.android.package-archive");
               // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);*/


                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                File mFile = new File(Environment.getExternalStorageDirectory()
                        + "/DepStore/",  obj.getApk());
                Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", mFile);
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
                finish();
                return intent;
            } catch (IOException e) {

              Log.e("Update error!", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Intent intent) {
            super.onPostExecute(intent);
        /*    if (listener != null) listener.run();
            listener = null;*/
        }
    };

    public void onBackPressed() {
        Intent intent = new Intent(pruebaapp.this, MainActivity.class);
        intent.putExtra("finish", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        startActivity(intent);
        pruebaapp.this.finish();
    }
}



