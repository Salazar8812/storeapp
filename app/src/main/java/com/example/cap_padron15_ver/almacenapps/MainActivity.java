package com.example.cap_padron15_ver.almacenapps;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.LogRecord;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class MainActivity extends AppCompatActivity {
    ListView lista;
    String error;
   // private List<aplicacion> listaapp;
    private aplicacion aplicaciones;
    private int posicion=0;
    public EditText dni;
    private ArrayList<aplicacion> listaapp;
    private static ArrayList<detalleapp> nombresapp= new ArrayList < detalleapp >();
    public EditText nombre;
    public EditText telefono;
    MyCustomAdapter dataAdapter = null;
    ProgressDialog progdialog;
    MainActivity act;
    infoappsintall dapp= new infoappsintall();
    Handler handler;
    SQLiteDatabase basededatos;
    datosestaticos obj= new datosestaticos();


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    static final String TAG = "Depstore";
    public static final String EXTRA_MESSAGE = "message";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String PROPERTY_EXPIRATION_TIME = "onServerExpirationTimeMs";
    private static final String PROPERTY_USER = "user";

    public static final long EXPIRATION_TIME_MS = 1000 * 3600 * 24 * 7;

    static String SENDER_ID = "494908600928";


    static String id_device;
    private Context context;
    private String regid;
    private GoogleCloudMessaging gcm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lista=(ListView)findViewById(R.id.list);
        TextView mensaje= (TextView)findViewById(R.id.men);
        Button intentar=(Button)findViewById(R.id.boton);

        getInstalledApps(false);
        new Mostrar().execute();
        listaapp = new ArrayList<aplicacion>();
        id_device= Settings.Secure.getString(getApplication().getContentResolver(), Settings.Secure.ANDROID_ID);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // When clicked, show a toast with the TextView text
                aplicacion c = (aplicacion) parent.getItemAtPosition(position);
                obj.setIdaplicacion(c.getIdaplicacion());
                obj.setNombre(c.getNombre());
                obj.setVersion(c.getVersion());
                obj.setFecha(c.getFecha());
                obj.setUrl(c.getUrl());
                obj.setIcono(c.getIcono());
                obj.setApk(c.getApk());
                obj.setDescrip(c.getDescripcion());
                obj.setAndroid_version(c.getVersion_android());
                Intent intent = new Intent(MainActivity.this, pruebaapp.class);
                startActivity(intent);
                MainActivity.this.finish();
                onPause();
            }
        });
        try {
            context = getApplicationContext();

            //Chequemos si est? instalado Google Play Services
            //if(checkPlayServices())
            //{
            gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
            //Obtenemos el Registration ID guardado
            regid = getRegistrationId(context);
            //Si no disponemos de Registration ID comenzamos el registro
            if (regid.equals("")) {

                // hiloregistradevice();
                TareaRegistroGCM tarea = new TareaRegistroGCM();
                tarea.execute("user");
            }
        }catch(Exception e){
            String error;
            error=e.toString();
            Toast.makeText(getApplication(),"Error"+error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.about) {
            Intent intent= new Intent(this, about.class );
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private String mostrar(){
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://agendaws.gearhostpreview.com/Depstore/datosapp.php");
        String resultado="";
        HttpResponse response;
        try {
            response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            InputStream instream = entity.getContent();
            resultado= convertStreamToString(instream);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return resultado;
    }

    private String convertStreamToString(InputStream is) throws IOException {
        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            }
            finally {
                is.close();
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    private boolean filtrarDatos(){

        //listaapp.clear();
        String data=mostrar();
        if(!data.equalsIgnoreCase("")){
            JSONObject json;
            try {
                json = new JSONObject(data);
                JSONArray jsonArray = json.optJSONArray("aplicacion");
                for (int i = 0; i < jsonArray.length(); i++) {
                    aplicaciones=new aplicacion();
                    JSONObject jsonArrayChild = jsonArray.getJSONObject(i);
                    aplicaciones.setIdaplicacion(jsonArrayChild.optString("idaplicacion"));
                    aplicaciones.setNombre(jsonArrayChild.optString("nombre"));
                    aplicaciones.setVersion(jsonArrayChild.optString("version"));
                    aplicaciones.setFecha(jsonArrayChild.optString("fecha"));
                    aplicaciones.setIcono(jsonArrayChild.optString("icono"));
                    aplicaciones.setUrl(jsonArrayChild.optString("url"));
                    aplicaciones.setApk(jsonArrayChild.optString("apk"));
                    aplicaciones.setDescripcion(jsonArrayChild.optString("descripcion"));
                    aplicaciones.setVersion_android(jsonArrayChild.optString("version_android"));
                    listaapp.add(aplicaciones);
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return true;
        }

        return false;
    }
    private void mostrarPersona(final int posicion){
        runOnUiThread(new Runnable(){
            @Override
            public void run() {
                // TODO Auto-generated method stub
               // aplicacion aplicaciones=listaapp.get(posicion);
                dataAdapter = new MyCustomAdapter(MainActivity.this, R.layout.activity_item,listaapp);
                lista.setAdapter(dataAdapter);

              /*  nombre.setText(aplicaciones.getNombre());
                dni.setText(aplicaciones.getIdaplicacion());
                telefono.setText(aplicaciones.getUrl());*/
            }
        });
    }
    class Mostrar extends AsyncTask<String,String,String> {
        protected void onPreExecute() {
            super.onPreExecute();
            progdialog = ProgressDialog.show(MainActivity.this,
                    "Dep Store", "Por favor espere...");
        }
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            if (filtrarDatos()) {
                return "1";
            }else{
                return "0";
            }
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result == "1") {
                mostrarPersona(posicion);
                progdialog.dismiss();
            } else {

            }
        }
    }

    private class MyCustomAdapter extends ArrayAdapter<aplicacion> {

        private ArrayList<aplicacion> clista;
        private int mSelectedPosition = -1;
        private int mResourceId = 0;

        public MyCustomAdapter(Context context, int resource, ArrayList<aplicacion> clista) {
            super(context, resource,clista);
            this.clista = new ArrayList<aplicacion>();
            this.clista.addAll(clista);
        }
        private class ViewHolder {
            TextView titulo;
            TextView version;
            TextView descarga;
            ImageView app;
            TextView tipo;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.activity_item, null);
                holder = new ViewHolder();
                holder.titulo = (TextView) convertView.findViewById(R.id.titulo);
                holder.version = (TextView) convertView.findViewById(R.id.version);
                holder.descarga = (TextView) convertView.findViewById(R.id.descargar);
                holder.tipo = (TextView) convertView.findViewById(R.id.actualizar);
                holder.app=(ImageView) convertView.findViewById(R.id.app);
                convertView.setTag(holder);
               /* holder.nom.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Button cb = (Button) v;
                    }
                });*/
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }
            aplicacion cl = clista.get(position);
// show The Image
            new DownloadImageTask(holder.app)
                    .execute("http://agendaws.gearhostpreview.com/Depstore/imagen.php?id="+cl.getIdaplicacion());
            holder.titulo.setText(cl.getNombre());
            holder.version.setText("Version "+cl.getVersion());
            holder.descarga.setText(cl.getFecha());
            if(appsinstaladas(cl.getNombre())){
                holder.tipo.setText("INSTALADA");
                if(appsversion(cl.getVersion(),cl.getNombre())) {
                    holder.tipo.setText("ACTUALIZAR");
                }
            }else{
                holder.tipo.setText("DESCARGAR");
            }
            return convertView;
        }
    }

class DownloadImageTask extends AsyncTask <String, Void, Bitmap>{
    ImageView bmImage;

    public DownloadImageTask(ImageView bmImage){
        this.bmImage = bmImage;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;

        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }
}

    private String getRegistrationId(Context context)
    {
        SharedPreferences prefs = getSharedPreferences(
                MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);

    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
    if (registrationId.length() == 0) {
        Log.d(TAG, "Registro GCM no encontrado.");
        return "";
    }

    String registeredUser =
            prefs.getString(PROPERTY_USER, "user");

    int registeredVersion =
            prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);

    long expirationTime =
            prefs.getLong(PROPERTY_EXPIRATION_TIME, -1);

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
    String expirationDate = sdf.format(new Date(expirationTime));

    Log.d(TAG, "Registro GCM encontrado (usuario=" + registeredUser +
            ", version=" + registeredVersion +
            ", expira=" + expirationDate + ")");

    int currentVersion = getAppVersion(context);

    if (registeredVersion != currentVersion) {
        Log.d(TAG, "Nueva versi�n de la aplicaci�n.");
        return "";
    } else if (System.currentTimeMillis() > expirationTime) {
        Log.d(TAG, "Registro GCM expirado.");
        return "";
    } else if (!id_device.equals(registeredUser)) {
        Log.d(TAG, "Nuevo nombre de usuario.");
        return "";
    }

        return registrationId;
    }

    private static int getAppVersion(Context context)
    {
        try
        {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);

            return packageInfo.versionCode;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            throw new RuntimeException("Error al obtener versi?n: " + e);
        }
    }

   private class TareaRegistroGCM extends AsyncTask<String,Integer,String>
    {
        @Override
        protected String doInBackground(String... params)
        {
            String msg = "";

            try
            {
                if (gcm == null)
                {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                //Nos registramos en los servidores de GCM
                regid =gcm.register(SENDER_ID);

                Log.d(TAG, "Registrado en GCM: registration_id=" + regid);
               // Toast.makeText(getApplication(),"Registrado"+regid,Toast.LENGTH_LONG).show();
                //Nos registramos en nuestro servidor
                boolean registrado = registroServidor(params[0],regid);

                //Guardamos los datos del registro
                if(registrado)
                {
                    setRegistrationId(context, params[0], regid);
                }
            }
            catch (Exception ex)
            {
                Log.d(TAG, "Error registro en GCM:" + ex.getMessage());
                String error;
                error=ex.toString();

            }

            return msg;
        }
    }

    private void setRegistrationId(Context context, String user, String regId)
    {
        SharedPreferences prefs = getSharedPreferences(
                MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);

        int appVersion = getAppVersion(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_USER, user);
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.putLong(PROPERTY_EXPIRATION_TIME,
                System.currentTimeMillis() + EXPIRATION_TIME_MS);

        editor.commit();
    }

    private boolean registroServidor(String usuario,String regID)
    {
        boolean reg = false;

        final String NAMESPACE = "urn:Servidor";
        final String URL="http://agendaws.gear.host/server.php";
        final String METHOD_NAME = "registradis";
        final String SOAP_ACTION = "urn:Servidor";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        request.addProperty("usuario", regID);
        request.addProperty("registro", id_device);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);

        try
        {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);

            String res = envelope.getResponse().toString();

            if(res.equals("1"))
            {
                Log.d(TAG, "Registrado en mi servidor.");
                reg = true;
            }
        }
        catch (Exception e)
        {


            error=e.toString();
            Log.d(TAG, "Error registro en mi servidor: " + e.getCause() + " || " + e.getMessage());
        }

        return reg;
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }


  class  detalleapp  {
        private  String appname =  "" ;
        private  String pname =  "" ;
        private  String versionName =  "" ;
        private  int versionCode =  0 ;
        private Drawable icon ;
        private  void prettyPrint ()  {
            //Log.v(appname + "t" + pname + "t" + versionName + "t" + versionCode);
        }
    }

    /*private ArrayList< detalleapp > getPackages ()  {
        ArrayList < detalleapp > apps = getInstalledApps ( false );  /* falsas = no hay paquetes del sistema
        final  int max = apps . size ();
        for  ( int i = 0 ; i < max ; i ++)  {
            apps . get ( i ). prettyPrint ();
        }
        return apps ;
    }*/

    private  void getInstalledApps ( boolean getSysPackages)  {
        ArrayList< detalleapp > res =  new  ArrayList < detalleapp >();
        List<PackageInfo> packs = getPackageManager (). getInstalledPackages(0);
        for ( int i = 0 ; i < packs . size (); i ++)  {
            PackageInfo p = packs . get ( i );
            if  ((! getSysPackages )  &&  ( p . versionName ==  null ))  {
                continue  ;
            }
            detalleapp newInfo =  new  detalleapp ();
            newInfo.appname = p . applicationInfo . loadLabel ( getPackageManager ()). toString ();
            newInfo.pname = p . packageName ;
            newInfo.versionName = p . versionName ;
            newInfo.versionCode = p . versionCode ;
            newInfo.icon = p . applicationInfo . loadIcon ( getPackageManager ());
         /*   if(newInfo.appname.equals(nom)) {
                m="0";
                i=packs.size();
            }*/
            nombresapp . add ( newInfo );
        }
       /* if(m=="0") {
            return true;
        }else {
            return false;
        }*/
        //return res ;
    }

    public boolean appsinstaladas(String nom){
        boolean fal=false;
        for(int i=0;i<=nombresapp.size()-1;i++){
            if(nombresapp.get(i).appname.equals(nom)) {
              fal=true;
               i=nombresapp.size();
            }else{
                fal=false;
            }
        }
        return fal;
    }
    public boolean appsversion(String version, String no ){
        boolean b=false;
        for(int i=0;i<=nombresapp.size()-1;i++){
            if(!nombresapp.get(i).versionName.equals(version) && nombresapp.get(i).appname.equals(no)) {
                b=true;
                i=nombresapp.size();
            }else {
                b=false;
        }
        }
       return b;
    }
}
