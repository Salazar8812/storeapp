package com.example.cap_padron15_ver.almacenapps;

/**
 * Created by cap_padron15_ver on 04/06/2015.
 */
public class datosestaticos {
    static String id;
    static String nombre;
    static String version;
    static String fecha;
    static String icono;
    static String url;
    static String apk;
    static boolean bp=false;

    public String getAndroid_version() {
        return android_version;
    }

    public void setAndroid_version(String android_version) {
        datosestaticos.android_version = android_version;
    }

    static String android_version;

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        datosestaticos.descrip = descrip;
    }

    static String descrip;

    public void setIdaplicacion(String idaplicacion) {
        this.id=idaplicacion;
    }
    public String getIdaplicacion(){
        return id;
    }

    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public String getNombre(){
        return nombre;
    }

    public void setVersion(String version){
        this.version=version;
    }
    public String getVersion(){
        return version;
    }

    public void setFecha(String fecha) {
        this.fecha= fecha;
    }
    public String getFecha(){
        return fecha;
    }

    public void setIcono(String icono){
        this.icono=icono;
    }
    public String getIcono(){
        return icono;
    }

    public void setUrl(String url){
        this.url=url;
    }
    public String getUrl(){
        return url;
    }

    public void setApk(String apk){
        this.apk=apk;
    }
    public String getApk() {
        return apk;
    }

    public void setBp(boolean bp2){
        bp=bp2;
    }
    public boolean getBp(){
        return bp;
    }
}
