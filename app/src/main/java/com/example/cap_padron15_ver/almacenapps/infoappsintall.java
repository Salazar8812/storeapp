package com.example.cap_padron15_ver.almacenapps;

/**
 * Created by cap_padron15_ver on 18/06/2015.
 */
public class infoappsintall {
    static String nombre;
    static String versionname;
    static String versioncode;
    static String paquete;
    static String tipo;

    public void setVersionname(String idaplicacion) {
        this.versionname=idaplicacion;
    }
    public String getVersionname(){
        return versionname;
    }
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public String getNombre(){
        return nombre;
    }
    public void setVersioncode(String version){
        this.versioncode=version;
    }
    public String getVersioncode(){
        return versioncode;
    }
    public void setPaquete(String fecha) {
        this.paquete= fecha;
    }
    public String getPaquete(){
        return paquete;
    }

    public void setTipo(String ti){
        tipo= ti;
    }
    public String getTipo(){
        return tipo;
    }
}
