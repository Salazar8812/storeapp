package com.example.cap_padron15_ver.almacenapps;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by cap_padron15_ver on 04/06/2015.
 */
public class appinfo extends AppCompatActivity {
    datosestaticos obj= new datosestaticos();
    private long enqueue;
    private long id;
   // private DownloadManager dm;
    ProgressBar pb;
    Dialog dialog;
    int downloadedSize = 0;
    int totalSize = 0;
    TextView cur_val;
    Button descargar;
    ProgressDialog progress;
    private static final String DL_ID = "downloadId";
    private SharedPreferences prefs;
    private DownloadManager dm;
    private Handler handler = new Handler();
    private View mActualizada, mActualizacion, mProgressBar;
    private TextView mVersionActual, mVersionNueva;
    private Button mObtenerDatos, mDescargar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appinfo);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.back);
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView nombre=(TextView)findViewById(R.id.nom);
        TextView version=(TextView)findViewById(R.id.ver);
        TextView fecha=(TextView)findViewById(R.id.act);
        ImageView pict=(ImageView)findViewById(R.id.icono);
        descargar=(Button)findViewById(R.id.descarga);

        //TextView enlace= (TextView)findViewById(R.id.liga);

      //  enlace.setText(Html.fromHtml("<a href=http://www.Depstore.gear.host>http://www.Depstore.gear.host</a>"));
        nombre.setText(obj.getNombre());
        version.setText(obj.getVersion());
        fecha.setText(obj.getFecha());
        Log.e("URL:",obj.getApk());
        new DownloadImageTask(pict)
                .execute("http://agendaws.gearhostpreview.com/Depstore/imagen.php?id=" + obj.getIdaplicacion());

        descargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //descargar.setBackgroundColor(Color.parseColor("#ffec585f"));
              /*  showProgress(obj.getUrl());
                new Thread(new Runnable() {
                    public void run() {
                        downloadFile();
                    }
                }).start();*/
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(obj.getUrl())));
               //descargaarchivos();
               // Toast.makeText(getApplication(),"Descargando...",Toast.LENGTH_LONG).show();
            }
        });

    }
    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage){
            this.bmImage = bmImage;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;

            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


 public boolean onCreateOptionsMenu(Menu menu) {
     // Inflate the menu; this adds items to the action bar if it is present.
     getMenuInflater().inflate(R.menu.menu_install, menu);
     return true;
 }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if ((id==android.R.id.home)) {

            Intent intent = new Intent(getApplication(), MainActivity.class);
            intent.putExtra("finish", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
            startActivity(intent);
            // app icon in action bar clicked; goto parent activity.
            return true;
        }
        if ((id==R.id.instalar)) {

            Intent intent = new Intent();
            intent.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
            startActivity(intent);
            // app icon in action bar clicked; goto parent activity.
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
 void downloadFile(){

     try {
         URL url = new URL(obj.getUrl());
         HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

         urlConnection.setRequestMethod("GET");
         urlConnection.setDoOutput(true);

         //connect
         urlConnection.connect();

         //set the path where we want to save the file
         File SDCardRoot = Environment.getExternalStorageDirectory();
         //create a new file, to save the <span id="IL_AD4" class="IL_AD">downloaded</span> file
         File file = new File(SDCardRoot,obj.getApk());

         FileOutputStream fileOutput = new FileOutputStream(file);

         //Stream used for reading the data from the internet
         InputStream inputStream = urlConnection.getInputStream();

         //this is the total size of the file which we are downloading
         totalSize = urlConnection.getContentLength();

         runOnUiThread(new Runnable() {
             public void run() {
                 pb.setMax(totalSize);
             }
         });

         //create a buffer...
        byte[] buffer = new byte[1024];
         int bufferLength = 0;

         while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
             fileOutput.write(buffer, 0, bufferLength);
             downloadedSize += bufferLength;
             // update the progressbar //
             runOnUiThread(new Runnable() {
                 public void run() {
                     pb.setProgress(downloadedSize);
                     float per = ((float)downloadedSize/totalSize) * 100;
                     cur_val.setText("Descargando " + downloadedSize + "KB / " + totalSize + "KB (" + (int)per + "%)" );
                    // descargar.setBackgroundColor(Color.parseColor("#ffec585f"));
                 }
             });
         }
         //close the output stream when complete //
         fileOutput.close();
         runOnUiThread(new Runnable() {
             public void run() {
                 // pb.dismiss(); // if you want close it..
             }
         });

     } catch (final MalformedURLException e) {
         showError("Error : MalformedURLException " + e);
         e.printStackTrace();
     } catch (final IOException e) {
         showError("Error : IOException " + e);
         e.printStackTrace();
     }
     catch (final Exception e) {
         showError("Error : Porfavor Verifica si estas conectado a Internet" + e);
     }
 }

    void showError(final String err){
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getApplication(), err, Toast.LENGTH_LONG).show();
            }
        });
    }

    void showProgress(String file_path){
        dialog = new Dialog(appinfo.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.myprogressdialog);
        dialog.setTitle("Descarga en Progreso");

        TextView text = (TextView) dialog.findViewById(R.id.tv1);
        text.setText("Descargando Aplicacion " + obj.getNombre()+"...");
        cur_val = (TextView) dialog.findViewById(R.id.cur_pg_tv);
        cur_val.setText("Comenzando Descarga...");
        dialog.show();

        pb = (ProgressBar)dialog.findViewById(R.id.progress_bar);
        pb.setProgress(0);
        pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));
    }
    public void descargaarchivos() {
        String dir = Environment.DIRECTORY_DOWNLOADS;

        File fileDir = new File(dir);
        if (!fileDir.isDirectory()) {
            fileDir.mkdir();
        }

       /* File direct = new File(Environment.getExternalStorageDirectory()
                + "/DepStore");

        if (!direct.exists()) {
            direct.mkdirs();
        }*/
       // Toast.makeText(Detail.this, "Download song " + name, Toast.LENGTH_SHORT).show();
        // Download File
        DownloadManager.Request request =
                new DownloadManager.Request(Uri.parse(obj.getUrl()));
        request.setDescription(obj.getApk());
        request.setTitle(obj.getNombre());
        // in order for this if to run, you must use the android 3.2 to compile your app
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }

        request.setDestinationInExternalPublicDir(dir, obj.getApk());
        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);

      /*  DownloadManager mgr = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);

        Uri downloadUri = Uri.parse(obj.getUrl());
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle(obj.getApk())
                .setVisibleInDownloadsUi(true)
                .setShowRunningNotification(true)
                .setDescription("Decargando...")
                .setDestinationInExternalPublicDir("/DepStore", obj.getApk());
        mgr.enqueue(request);*/
    }
    // Runnable que se ejecutará desde el hilo principal cuando acabe la
    // obtención de datos en segundo plano. Equivalente al onPostExecute() de AsyncTask
    private Runnable finishBackgroundDownload = new Runnable() {
        @Override
        public void run() {
            mProgressBar.setVisibility(View.GONE);
          /*  if(mVC.isNewVersionAvailable()){
                // Hay una nueva versión disponible
                mActualizacion.setVisibility(View.VISIBLE);
                mActualizada.setVisibility(View.GONE);
                // Mostramos los datos
                mVersionActual.setText("Versión actual: " + mVC.getCurrentVersionName());
                mVersionNueva.setText("Versión disponible: " + mVC.getLatestVersionName());
            }else{
                // La aplicación está actualizada
                mActualizacion.setVisibility(View.GONE);
                mActualizada.setVisibility(View.VISIBLE);
            }*/
        }
    };

    // Runnable encargado de descargar los datos en un hilo en segundo plano.
    // Equivalente al doInBackground() de AsyncTask
    private Runnable backgroundDownload = new Runnable() {
        @Override
        public void run() {
            //  mVC.getData(pruebaapp.this);
            // Cuando acabe la descarga actualiza la interfaz
            handler.post(finishBackgroundDownload);
        }
    };
}

