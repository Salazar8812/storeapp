package com.example.cap_padron15_ver.almacenapps;

/**
 * Created by cap_padron15_ver on 03/06/2015.
 */
public class aplicacion {

    private String idaplicacion;
    private String nombre;
    private String version;
    private String fecha;
    private String icono;
    private String url;
    private String apk;

    public String getVersion_android() {
        return version_android;
    }

    public void setVersion_android(String version_android) {
        this.version_android = version_android;
    }

    private String version_android;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    private String descripcion;

    public void setIdaplicacion(String idaplicacion) {
    this.idaplicacion=idaplicacion;
    }
    public String getIdaplicacion(){
        return idaplicacion;
    }

    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public String getNombre(){
        return nombre;
    }

    public void setVersion(String version){
        this.version=version;
    }
    public String getVersion(){
        return version;
    }

    public void setFecha(String fecha) {
    this.fecha= fecha;
    }
    public String getFecha(){
        return fecha;
    }

    public void setIcono(String icono){
        this.icono=icono;
    }
    public String getIcono(){
        return icono;
    }

    public void setUrl(String url){
        this.url=url;
    }
    public String getUrl(){
        return url;
    }

    public void setApk(String apk){
        this.apk=apk;
    }
    public String getApk() {
    return apk;
    }
}
